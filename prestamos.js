var DIARIO = "diario";
var SEMANAL = "semanal";
var QUINCENAL = "quincenal";
var MENSUAL = "mensual";
var BIMESTRAL = "bimestral";
var TRIMESTRAL = "trimestral";
var CUATRIMESTRAL = "cuatrimestral";
var SEMESTRAL = "semestral";
var ANUAL = "anual";

function getTasa(tasa, tasa_tipo, periodo) {
    if (tasa_tipo == ANUAL) { tasa = tasa / 12 }
    tasa = tasa / 100.0
    if (periodo == DIARIO) { tasa = tasa / 30.4167 };
    if (periodo == SEMANAL) { tasa = tasa / 4.34524 };
    if (periodo == QUINCENAL) { tasa = tasa / 2 };
    if (periodo == MENSUAL) { };
    if (periodo == BIMESTRAL) { tasa = tasa * 2 };
    if (periodo == TRIMESTRAL) { tasa = tasa * 3 };
    if (periodo == CUATRIMESTRAL) { tasa = tasa * 4 };
    if (periodo == SEMESTRAL) { tasa = tasa * 6 };
    if (periodo == ANUAL) { tasa = tasa * 12 };
    return tasa;
}

function getValorDeCuotaFija(monto, tasa, cuotas, periodo, tasa_tipo) {
    tasa = getTasa(tasa, tasa_tipo, periodo);
    valor = monto *( (tasa * Math.pow(1 + tasa, cuotas)) / (Math.pow(1 + tasa, cuotas) - 1) );
    return valor.toFixed(2);
}

function getAmortizacion(monto, tasa, cuotas, periodo, tasa_tipo) {
    var valor_de_cuota = getValorDeCuotaFija(monto, tasa, cuotas, periodo, tasa_tipo);
    var saldo_al_capital = monto;
    var items = new Array();

    for (i=0; i < cuotas; i++) {
        interes = saldo_al_capital * getTasa(tasa, tasa_tipo, periodo);
        abono_al_capital = valor_de_cuota - interes;
        saldo_al_capital -= abono_al_capital;
        numero = i + 1;
        
        interes = interes.toFixed(2);
        abono_al_capital = abono_al_capital.toFixed(2);
        saldo_al_capital = saldo_al_capital.toFixed(2);

        item = [numero, interes, abono_al_capital, valor_de_cuota, saldo_al_capital];
        items.push(item);
    }
    return items;
}


function setMoneda(num) {
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num)) num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10) cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + '$' + num + ((cents == "00") ? '' : '.' + cents));
}





        function calcular() {
            var monto = document.getElementById("input_monto").value;
            var cuotas = document.getElementById("input_cuotas").value;
            var tasa = document.getElementById("input_tasa").value;
            if (!monto) {
                alert('Indique el monto');
                return;
            }
            if (!cuotas) {
                alert('Indique las cuotas');
                return;
            }
            if (!tasa) {
                alert('Indique la tasa');
                return;
            }
            if (parseInt(cuotas) < 1) {
                alert('Las cuotas deben ser de 1 en adelante');
                return;
            }
            var select_periodo = document.getElementById("select_periodo");
            periodo = select_periodo.options[select_periodo.selectedIndex].value;
            var select_tasa_tipo = document.getElementById("select_tasa_tipo");
            tasa_tipo = select_tasa_tipo.options[select_tasa_tipo.selectedIndex].value;
            var items = getAmortizacion(monto, tasa, cuotas, periodo, tasa_tipo);
            var tbody = document.getElementById("tbody_1");
            tbody.innerHTML = "";
  

if (parseInt(cuotas) > 3000) { alert("Ha indicado una cantidad excesiva de cuotas, porfavor reduzcala a menos de 3000"); return; }



            for (i = 0; i < items.length; i++) {
                item = items[i];
                tr = document.createElement("tr");
                for (e = 0; e < item.length; e++) {
                    value = item[e];
                    if (e > 0) { value = setMoneda(value); }
                    td = document.createElement("td");
                    textCell = document.createTextNode(value);
                    td.appendChild(textCell);
                    tr.appendChild(td);
                }
                tbody.appendChild(tr);
            }
            var div1 = document.getElementById("div-valor-cuota");

            valor = setMoneda(items[0][3]);
            div1.innerHTML = valor;
            var msg = "";
            if (periodo == "diario") { 
    msg = "Usted estará pagando " + valor + ", todos los dias durante " + items.length + " dias.";
   }
   if (periodo == "semanal") {
    msg = "Usted pagará " + valor + ", semanalmente durante " + items.length + " semanas.";
   }
   if (periodo == "mensual") {
    msg = "Usted pagará " + valor + ", mensualmente durante " + items.length + " meses.";
   }
   if (periodo == "quincenal") {
    msg = "Usted pagará " + valor + ", de manera quincenal por un periodo de " + items.length + " quincenas.";
   }
   if (periodo == "bimestral") {
    msg = "Usted pagará " + valor + ", cada 2 meses durante un periodo de " + items.length + " bimestres.";
   }
   if (periodo == "trimestral") {
    msg = "Usted va a pagar " + valor + ", cada 3 meses durante " + items.length + " trimestres.";
   }
   if (periodo == "cuatrimestral") {
    msg = "Usted pagará " + valor + ", cada cuatrimestre (4 meses) por un periodo de " + items.length + " cuatrimestres.";
   }
   if (periodo == "semestral") {
    msg = "Usted pagará " + valor + ", cada 6 meses durante " + items.length + " semestres";
   }
   if (periodo == "anual") {
    msg = "Usted pagará " + valor + ", anualmente por un periodo de " + items.length + " años";
   }
   var div2 = document.getElementById("div-comentario");
   div2.innerHTML = msg;
        }

<script type='text/javascript'>
      window.setTimeout(function() {
        document.body.className = document.body.className.replace('loading', '');
                                                                  }, 10);
    </script>

<script type="text/javascript" src="https://www.blogger.com/static/v1/widgets/2575128383-widgets.js"></script>
<script type='text/javascript'>
var BLOG_BASE_IMAGE_URL = 'https://resources.blogblog.com/img';var BLOG_LANG_DIR = 'ltr';window['__wavt'] = 'AOuZoY7sgxJgN9YaG27EIKw2I-zhWz5VDg:1566563178991';_WidgetManager._Init('//www.blogger.com/rearrange?blogID\x3d8305180851420374787','//holamundopy.blogspot.com/2016/02/javascript-calculadora-de-prestamos.html?m\x3d1','8305180851420374787');
_WidgetManager._SetDataContext([{'name': 'blog', 'data': {'blogId': '8305180851420374787', 'title': 'Hola Mundo', 'url': 'https://holamundopy.blogspot.com/2016/02/javascript-calculadora-de-prestamos.html?m\x3d1', 'canonicalUrl': 'https://holamundopy.blogspot.com/2016/02/javascript-calculadora-de-prestamos.html', 'homepageUrl': 'https://holamundopy.blogspot.com/?m\x3d1', 'searchUrl': 'https://holamundopy.blogspot.com/search', 'canonicalHomepageUrl': 'https://holamundopy.blogspot.com/', 'blogspotFaviconUrl': 'https://holamundopy.blogspot.com/favicon.ico', 'bloggerUrl': 'https://www.blogger.com', 'hasCustomDomain': false, 'httpsEnabled': true, 'enabledCommentProfileImages': true, 'gPlusViewType': 'FILTERED_POSTMOD', 'adultContent': false, 'analyticsAccountNumber': 'UA-88387660-2', 'encoding': 'UTF-8', 'locale': 'es', 'localeUnderscoreDelimited': 'es', 'languageDirection': 'ltr', 'isPrivate': false, 'isMobile': true, 'isMobileRequest': true, 'mobileClass': ' mobile', 'isPrivateBlog': false, 'feedLinks': '\x3clink rel\x3d\x22alternate\x22 type\x3d\x22application/atom+xml\x22 title\x3d\x22Hola Mundo - Atom\x22 href\x3d\x22https://holamundopy.blogspot.com/feeds/posts/default\x22 /\x3e\n\x3clink rel\x3d\x22alternate\x22 type\x3d\x22application/rss+xml\x22 title\x3d\x22Hola Mundo - RSS\x22 href\x3d\x22https://holamundopy.blogspot.com/feeds/posts/default?alt\x3drss\x22 /\x3e\n\x3clink rel\x3d\x22service.post\x22 type\x3d\x22application/atom+xml\x22 title\x3d\x22Hola Mundo - Atom\x22 href\x3d\x22https://www.blogger.com/feeds/8305180851420374787/posts/default\x22 /\x3e\n\n\x3clink rel\x3d\x22alternate\x22 type\x3d\x22application/atom+xml\x22 title\x3d\x22Hola Mundo - Atom\x22 href\x3d\x22https://holamundopy.blogspot.com/feeds/2865286759816958348/comments/default\x22 /\x3e\n', 'meTag': '', 'adsenseClientId': 'ca-pub-6132554739848284', 'adsenseHostId': 'ca-host-pub-1556223355139109', 'adsenseHasAds': true, 'ieCssRetrofitLinks': '\x3c!--[if IE]\x3e\x3cscript type\x3d\x22text/javascript\x22 src\x3d\x22https://www.blogger.com/static/v1/jsbin/95993233-ieretrofit.js\x22\x3e\x3c/script\x3e\n\x3c![endif]--\x3e', 'view': '', 'dynamicViewsCommentsSrc': '//www.blogblog.com/dynamicviews/4224c15c4e7c9321/js/comments.js', 'dynamicViewsScriptSrc': '//www.blogblog.com/dynamicviews/2df76443987ae0d9', 'plusOneApiSrc': 'https://apis.google.com/js/plusone.js', 'disableGComments': true, 'sharing': {'platforms': [{'name': 'Obtener enlace', 'key': 'link', 'shareMessage': 'Obtener enlace', 'target': ''}, {'name': 'Facebook', 'key': 'facebook', 'shareMessage': 'Compartir en Facebook', 'target': 'facebook'}, {'name': 'Escribe un blog', 'key': 'blogThis', 'shareMessage': 'Escribe un blog', 'target': 'blog'}, {'name': 'Twitter', 'key': 'twitter', 'shareMessage': 'Compartir en Twitter', 'target': 'twitter'}, {'name': 'Pinterest', 'key': 'pinterest', 'shareMessage': 'Compartir en Pinterest', 'target': 'pinterest'}, {'name': 'Correo electrónico', 'key': 'email', 'shareMessage': 'Correo electrónico', 'target': 'email'}], 'disableGooglePlus': true, 'googlePlusShareButtonWidth': 300, 'googlePlusBootstrap': '\x3cscript type\x3d\x22text/javascript\x22\x3ewindow.___gcfg \x3d {\x27lang\x27: \x27es\x27};\x3c/script\x3e'}, 'hasCustomJumpLinkMessage': true, 'jumpLinkMessage': 'Continuar leyendo &#187;', 'pageType': 'item', 'postId': '2865286759816958348', 'postImageThumbnailUrl': 'https://3.bp.blogspot.com/-5mpkCwHTpCY/WCwJo9bdCQI/AAAAAAAAM-g/0Rtj5YwctGkxmzgoZcfMSw8DTGPdAtRfQCLcB/s72-c/Imagen1.jpg', 'postImageUrl': 'https://3.bp.blogspot.com/-5mpkCwHTpCY/WCwJo9bdCQI/AAAAAAAAM-g/0Rtj5YwctGkxmzgoZcfMSw8DTGPdAtRfQCLcB/s400/Imagen1.jpg', 'pageName': 'Javascript: Calculadora de préstamos', 'pageTitle': 'Hola Mundo: Javascript: Calculadora de préstamos', 'metaDescription': 'Aquí podrás realizar el cálculo de tu préstamo a cuota fija. Solo ingresa los datos necesarios y presiona el botón \x27Calcular\x27. El resultado será igual a la cuota fija que tendrás que pagar, y la tabla de amortización donde se te mostrará el detalle sobre capital e interés en cada una de las cuotas.'}}, {'name': 'features', 'data': {'sharing_get_link_dialog': 'true', 'sharing_native': 'false'}}, {'name': 'messages', 'data': {'edit': 'Editar', 'linkCopiedToClipboard': 'El enlace se ha copiado en el Portapapeles.', 'ok': 'Aceptar', 'postLink': 'Enlace de la entrada'}}, {'name': 'template', 'data': {'name': 'custom', 'localizedName': 'Personalizado', 'isResponsive': false, 'isAlternateRendering': true, 'isCustom': true}}, {'name': 'view', 'data': {'classic': {'name': 'classic', 'url': '?view\x3dclassic'}, 'flipcard': {'name': 'flipcard', 'url': '?view\x3dflipcard'}, 'magazine': {'name': 'magazine', 'url': '?view\x3dmagazine'}, 'mosaic': {'name': 'mosaic', 'url': '?view\x3dmosaic'}, 'sidebar': {'name': 'sidebar', 'url': '?view\x3dsidebar'}, 'snapshot': {'name': 'snapshot', 'url': '?view\x3dsnapshot'}, 'timeslide': {'name': 'timeslide', 'url': '?view\x3dtimeslide'}, 'isMobile': true, 'title': 'Javascript: Calculadora de préstamos', 'description': 'Aquí podrás realizar el cálculo de tu préstamo a cuota fija. Solo ingresa los datos necesarios y presiona el botón \x27Calcular\x27. El resultado será igual a la cuota fija que tendrás que pagar, y la tabla de amortización donde se te mostrará el detalle sobre capital e interés en cada una de las cuotas.', 'featuredImage': 'https://3.bp.blogspot.com/-5mpkCwHTpCY/WCwJo9bdCQI/AAAAAAAAM-g/0Rtj5YwctGkxmzgoZcfMSw8DTGPdAtRfQCLcB/s400/Imagen1.jpg', 'url': 'https://holamundopy.blogspot.com/2016/02/javascript-calculadora-de-prestamos.html?m\x3d1', 'type': 'item', 'isSingleItem': true, 'isMultipleItems': false, 'isError': false, 'isPage': false, 'isPost': true, 'isHomepage': false, 'isArchive': false, 'isLabelSearch': false, 'postId': 2865286759816958348}}]);
_WidgetManager._RegisterWidget('_HeaderView', new _WidgetInfo('Header1', 'header', document.getElementById('Header1'), {}, 'displayModeFull'));
_WidgetManager._RegisterWidget('_BlogView', new _WidgetInfo('Blog1', 'main', document.getElementById('Blog1'), {'cmtInteractionsEnabled': false, 'mobile': true}, 'displayModeFull'));
_WidgetManager._RegisterWidget('_AdSenseView', new _WidgetInfo('AdSense1', 'sidebar-right-1', document.getElementById('AdSense1'), {}, 'displayModeFull'));
_WidgetManager._RegisterWidget('_HTMLView', new _WidgetInfo('HTML3', 'sidebar-right-1', document.getElementById('HTML3'), {}, 'displayModeFull'));
_WidgetManager._RegisterWidget('_HTMLView', new _WidgetInfo('HTML1', 'sidebar-right-1', document.getElementById('HTML1'), {}, 'displayModeFull'));
_WidgetManager._RegisterWidget('_PopularPostsView', new _WidgetInfo('PopularPosts2', 'sidebar-right-1', document.getElementById('PopularPosts2'), {}, 'displayModeFull'));
_WidgetManager._RegisterWidget('_LabelView', new _WidgetInfo('Label1', 'sidebar-right-1', document.getElementById('Label1'), {}, 'displayModeFull'));
_WidgetManager._RegisterWidget('_HTMLView', new _WidgetInfo('HTML2', 'footer-1', document.getElementById('HTML2'), {}, 'displayModeFull'));
_WidgetManager._RegisterWidget('_PopularPostsView', new _WidgetInfo('PopularPosts1', 'footer-1', document.getElementById('PopularPosts1'), {}, 'displayModeFull'));
_WidgetManager._RegisterWidget('_AttributionView', new _WidgetInfo('Attribution1', 'footer-3', document.getElementById('Attribution1'), {}, 'displayModeFull'));

      window.setTimeout(function() {
        document.body.className = document.body.className.replace('loading', '');
                                                                  }, 10);
    </script>